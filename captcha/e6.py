!#/usr/bin/python3
import argparse
import os
import re
import urllib.request
import http.client
import pytesseract
from PIL import Image
import sys

def main():
    #parse commandline arguments and store it into ip_add.
    parser = argparse.ArgumentParser()
    parser.add_argument('-ip','--address', help='ip address')
    args = parser.parse_args()
    ip_add = args.address

    #connect to the webpage
    connection = http.client.HTTPConnection(ip_add)
    connection.request('GET','/captcha/example6/')

    #save the cookie to use again.
    response = connection.getresponse()
    cookie = re.findall(r'rack.session=(.*)',response.getheader('set-cookie'))
    headers = {'Cookie:' + cookie}

    #find and download the captcha.
    link = re.findall(r'<img src="(.*)"/ >', response.read().decode('utf-8'))[0]
    urllib.request.urlretrieve('http://'+ip_add+'/captcha/example6/'+link, 'temp.png')

    #parse captcha with tesseract.
    #pytesseract.image_to_string(Image.open('test.png'))
    captcha = pytesseract.image_to_string(Image.open('temp.png'))
    if (captcha.islower() and captcha.isalpha()):
        print("Successfully decoded captcha, sending back to server.")
        print('Captcha= {}'.format(captcha))
        connection.request('GET','submit?captcha='+captcha+'&submit=Submit',headers)
        #check the response to see if the server accepted the captcha.
        #download pillow and pytesseract for vm.
    else:
        print('Failed to decode captcha.')
        sys.exit(0)
