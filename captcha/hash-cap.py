import hashlib
import os

def main():
    directory = os.listdir('./captcha/e5-cap')

    for cap in directory:
        ext = cap.find('.png')
        if ext != -1:
            name = cap[0:ext]
            image = open('./captcha/e5-cap/' + cap,'rb')
            hash_img = hashlib.md5(image.read()).hexdigest()
            print("{0} : {1}".format(hash_img,name))
            image.close()

if __name__ == '__main__':
    main()
