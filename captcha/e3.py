import http.client
import re
import argparse

def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("-ip","--address",help="ip address")
    args = parser.parse_args()
    ip_add = args.address

    connection = http.client.HTTPConnection(ip_add)
    connection.request('GET','/captcha/example3/')
    response = connection.getresponse()
    match = re.findall(r'captcha=(.*)',response.getheader('set-cookie'))[0]
    print("Captcha: {}".format(match))

if __name__ == '__main__':
    main()
