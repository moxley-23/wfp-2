Auth 1:
In this example we try to bypass any authorization by just puting in the url to resource:

authorization/example1/infos/1
and
authorization/example1/infos/2

This will give you direct access to the resources, even when you arent logged in.

Auth 2:
In this example you can access other users files by simply incrementing the number at the end of the url.

authorization/example2/infos/3
and
authorization/example2/infos/4
These will work but nothing after that, youll get an internal server error.

Auth 3:
This example is similar to the attack above, however we need to exploit it in the edit page.

Log in and click on any of the posts and change the number at the end to 3.
You now have access to the information from user2.
